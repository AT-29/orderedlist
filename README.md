# OrderedList

Array implementation with  O(1) get by index, inser start, insert end, delete start, delete end


```
import OrderedList from './OrderedList.js';

const x = new OrderedList([0]);
x.is(-1);
x.ie(1);
x.is(-2);
x.ie(2);
x.ie(3);
x.ie(4);
x.is(-3);
x.is(-4);

x.is('start');
x.ie('end');

["start", -4, -3, -2, -1, 0, 1, 2, 3, 4, "end"] 
```

insert/remove at start vs array insert/remove at start 


```
list insert at start: 19.134033203125 ms
list remove at start: 8.677001953125 ms

array insert at start: 652.014892578125 ms
array remove at start: 554.856201171875 ms


300000 iterations  on repl.it PC

list insert at start: 716.534ms 
list remove at start: 29.140ms

list insert at end: 92.779ms
list remove at end: 80.550ms

list convert to array: 874.061ms

------------------------------

array insert at end: 82.937ms
array remove at end: 15.744ms

array insert at start: 130228.308ms
array remove at start: 116377.922ms
```


Benchmark test
```
import OrderedList from './OrderedList.js';

const array = [];
const list = new OrderedList();

console.clear();
const iterations = 100000
console.time("list insert at start");
for (let i = 0; i < iterations; i++){
  list.is(i)
}
console.timeEnd("list insert at start");

console.time("list remove at start");
for (let i = 0; i < iterations; i++){
  list.rs()
}
console.timeEnd("list remove at start");

console.time("array insert at start");
for (let i = 0; i < iterations; i++){
 array.unshift(i);
}
console.timeEnd("array insert at start");

console.time("array remove at start");
for (let i = 0; i < iterations; i++){
 array.shift();
}
console.timeEnd("array remove at start");
```
extentions
```
class listArray extends OrderedList {
  constructor(){
   super(); 
  }
  
  toArray() {
   const result = [];
   const elements = this._items.elements;
   for (let i = 0, len = elements.length; i < len; i++) {
      const values = elements[i];
      if(!values) continue;
      if (values[1] !== null) {
        result[i - this._offset_start] = values[1]; 
      }
      if (values[0] !== null) {
       result[i * -1 - this._offset_start] = values[0]; 
      } 
   }
    return result;
  }
  
  map(callback) {
  const result = new listArray();
  for(let i = 0; i < this.size; i++){
      result.ie(callback(this.get(i), i))
  }
    return result;
  }

   forEach(callback) {
    for(let i = 0; i < this.size; i++){
        callback(this.get(i), i)
    }
  }
}
```


