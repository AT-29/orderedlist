class Matrix {
  _elements = [];
  size = 0;

  abs(key) {
    return key < 0 ? key * -1 : key;
  }

  get size() {
    return this.size;
  }

  get elements() {
    return this._elements;
  }

  clear() {
    this._elements = [];
    this.size = 0;
  }

  set(key, value) {
    const index = this.abs(key);

    if (!this._elements[index]) {
      this._elements[index] = [null, null];
    }
    
    const current = this._elements[index];

    if (key >= 0) {
      current[1] = value;
    } else {
      current[0] = value;
    }
    this.size++;
  }

  get(key) {
    const index = this.abs(key);
    const current = this._elements[index];
    if (!current) return null;
    return key >= 0 ? current[1] : current[0];
  }

  delete(key) {
    const index = this.abs(key);
    const current = this._elements[index];
    if (!current) return null;
    const previous = this._elements[index-1] || [null, null];
    if (key >= 0) {
      current[1] = null;
    } else {
      current[0] = null;
    }
    if (current[0] === null && current[1] === null && 
       previous[0] !== null && previous[1] !== null) {
      this._elements.length--;
    }
    this.size--;
  }
}

export default class OrderedList {
  _offset_end = 0;
  _offset_start = 0;
  _items = new Matrix();

  constructor(initial) {
    if (typeof initial === "object") {
      if (!Array.isArray(initial)) {
        initial = Object.values(initial);
      }
      initial.map((item) => this.ie(item));
    }
  }

  get matrix() {
    return this._items.elements;
  }
  
  get(index) {
    return this._items.get(index + this._offset_start);
  }

  get size() {
    return this._items.size;
  }

  clear() {
    this._items.clear();
    this._offset_start = 0;
    this._offset_end = 0;
  }

  is(item) {
    this._items.set(--this._offset_start, item);
  }

  ie(item) {
    this._items.set(this._offset_end++, item);
  }

  rs() {
    if (this._items.size === 0) return;
    this._items.delete(this._offset_start++);
  }

  re() {
    if (this._items.size === 0) return;
    this._items.delete(--this._offset_end);
  }
}

